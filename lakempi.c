/* Group info:
 *
 * sananth5 Srinath Krishna Ananthakrishnan
 * sramach6 Subramanian Ramachandran
 * vchandr3 Vinoth Kumar Chandra Mohan
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <sys/time.h>

//#define DEBUG 1

#define _USE_MATH_DEFINES

#define XMIN 0.0
#define XMAX 1.0
#define YMIN 0.0
#define YMAX 1.0

#define MAX_PSZ 10
#define TSCALE 1.0
#define VSQR 0.1

extern void init_cuda(int anarea, int an, double *u0, double *u1,
                      double *pebs, int anthreads, int pt, int pb,
                      int pl, int pr);
extern void run_gpu(double *ti, double *bi, double *li, double *ri,
                    double h, double t, double dt);
extern void deinit_cuda(double *u);

/* so it works like this! :)
 *
 * the whole npoints * npoints grid is decomposed into size mpi blocks.
 * the size is expected to be a square of a power of two
 * as is the npoints is expected to be a power of two
 *
 * each of the mpi blocks are processed by one mpi task
 * each mpitask derives its x and y coords in the big picture from its rank and
 * the number of mpi tasks
 *
 * each task is now responsible for npoints * npoints / size points
 * this is the value narea and its sqrt is stored in n
 *
 * each task has its points numbered like this
 *   0   1  2  3  4 ... 63
 *   64 65 66 ...      127
 *   ...
 */

void init(double *u, double *pebbles, int n);
void init_pebbles_bcast(int rank, int *pebspos, int pn, int n);
void print_heatmap(char *filename, double *u, int n, double h);
double f(double p, double t);

int main(int argc, char *argv[])
{
  /* mpi specific variables */
  int size;   // number of tasks in all
  int rank;   // rank of this task
  int size1d; // number of tasks in one dimension = sqrt(size)
  int myx;    // x coord of the current mpi task
  int myy;    // y coord of the current mpi task
  int pl, pr, pb, pt; // my peers to the left, right, bottom and top

  /* others */
  int npoints;  // total number of points in the world
  int npebs;    // number of pebbles (used only at root)
  int nthreads; // number of cuda threads per block dimension
  int narea;    // total number of points within a task
  int n;        // number of points in a dimension within a task
  int i, j;

  double end_time; // time of simulation

  double *u_i0, *u_i1, *u;   // the actual grid points
  double *t1 = NULL;         // the edge points of a task
  double *b1 = NULL;         // the edge points of a task
  double *l1 = NULL;         // the edge points of a task
  double *r1 = NULL;         // the edge points of a task

  double *lbuf, *rbuf;       // used to send and receive left and right edge data

  int *pebspos;              // points of pebs, will be sent by root
  double *pebs;              // the pebble points

  double h;
  char file[20];

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  size1d = sqrt(size);

  /* arg checking */
  if(argc != 5) {
    if (rank == 0)
      printf("Usage: %s npoints npebs time_finish nthreads \n",argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  npoints = atoi(argv[1]);
  npebs = atoi(argv[2]);
  end_time = (double)atof(argv[3]);
  nthreads = atoi(argv[4]);

  n = npoints / size1d;
  narea	= n * n;

  u_i0 = (double*)malloc(sizeof(double) * narea);
  u_i1 = (double*)malloc(sizeof(double) * narea);
  u = (double*)malloc(sizeof(double) * narea);
  pebs = (double*)malloc(sizeof(double) * narea);

  lbuf = (double*) malloc(sizeof(double) * n);
  rbuf = (double*) malloc(sizeof(double) * n);

  pebspos = (int*) malloc(sizeof(int) * npebs);

  /* my x and y co-ordinates in the task graph */
  myx = rank % size1d;
  myy = rank / size1d;

#ifdef DEBUG
  printf("myx: %d, myy: %d\n", myx, myy);
#endif

  /* find out my peers */
  pl = myx ? rank - 1 : -1;
  pr = myx != size1d-1 ? rank + 1 : -1;
  pt = myy ? rank - size1d: -1;
  pb = myy != size1d-1 ? rank + size1d: -1;

#ifdef DEBUG
  printf("pt: %d, pb: %d, pl: %d, pr: %d\n", pt, pb, pl, pr);
#endif

  if (pt != -1)
    t1 = (double*) malloc(sizeof(double) * n);
  if (pb != -1)
    b1 = (double*) malloc(sizeof(double) * n);
  if (pl != -1)
    l1 = (double*) malloc(sizeof(double) * n);
  if (pr != -1)
    r1 = (double*) malloc(sizeof(double) * n);

  h = (XMAX - XMIN)/npoints;

  /* going to generate pebbles and bcast to everyone */
  memset(pebs, 0, sizeof(double) * narea);
  srand(time(NULL));

#ifdef DEBUG
  printf("Rank: %d, before broadcaast\n", rank);
#endif
  init_pebbles_bcast(rank, pebspos, npebs, npoints);
#ifdef DEBUG
  printf("Rank: %d, after broadcaast\n", rank);
#endif

  /* if pebbles fall in my range, find out their position
   * in the grid */
  int crdx, crdy;
  int prdx, prdy;
  for (i=0; i<npebs; i++) {
    crdx = (pebspos[i] % npoints) / n;
    crdy = (pebspos[i] / npoints) / n;
    if (crdx == myx && crdy == myy) {
      pebspos[i] = pebspos[i] - (crdx*n + crdy*npoints*n);
      prdx = pebspos[i] % npoints;
      prdy = pebspos[i] / npoints;
      pebs[prdy * n + prdx] = rand() % MAX_PSZ;
    }
  }

  /* initialise u_i0 and u_i1 */
  init(u_i0, pebs, n);
  init(u_i1, pebs, n);

  snprintf(file, 20, "lake_i%d.dat", rank);
  print_heatmap(file, u_i0, n, h);

#ifdef DEBUG
  printf("Rank: %d, before init cuda narea: %d, n: %d, nthreads: %d, u_i0: %p, u_i1: %p\n", rank, narea, n, nthreads, u_i0, u_i1);
#endif

  /* init cuda! - setup some memory */
  init_cuda(narea, n, u_i0, u_i1, pebs, nthreads, pt, pb, pl, pr);
#ifdef DEBUG
  printf("Rank: %d, after init cuda\n", rank);
#endif

  double t = 0.;
  double dt = h / 2.;
  MPI_Request req[8];
#ifdef DEBUG
  printf("Rank: %d, t1: %p, b1: %p, l1: %p, r1: %p, h: %lf, t: %lf, dt: %lf\n", rank, t1, b1, l1, r1, h, t, dt); 
#endif

  while (1) {
    i = 0;

    /* exchange boundary information if i have peers */
    if (pt != -1) {
      MPI_Isend(u_i1, n, MPI_DOUBLE, pt, 0, MPI_COMM_WORLD, &req[i++]);
      MPI_Irecv(t1, n, MPI_DOUBLE, pt, MPI_ANY_TAG, MPI_COMM_WORLD, &req[i++]);
    }
    if (pb != -1) {
      MPI_Isend(u_i1 + (n-1)*n, n, MPI_DOUBLE, pb, 1, MPI_COMM_WORLD, &req[i++]);
      MPI_Irecv(b1, n, MPI_DOUBLE, pb, MPI_ANY_TAG, MPI_COMM_WORLD, &req[i++]);
    }
    if (pl != -1) {
      for (j=0; j<n; j++) {
        lbuf[j] = u_i1[j*n];
      }
      MPI_Isend(lbuf, n, MPI_DOUBLE, pl, 2, MPI_COMM_WORLD, &req[i++]);
      MPI_Irecv(l1, n, MPI_DOUBLE, pl, MPI_ANY_TAG, MPI_COMM_WORLD, &req[i++]);
    }
    if (pr != -1) {
      for (j=0; j<n; j++) {
        rbuf[j] = u_i1[(j+1)*n-1];
      }
      MPI_Isend(rbuf, n, MPI_DOUBLE, pr, 3, MPI_COMM_WORLD, &req[i++]);
      MPI_Irecv(r1, n, MPI_DOUBLE, pr, MPI_ANY_TAG, MPI_COMM_WORLD, &req[i++]);
    }

    MPI_Waitall(i, req, MPI_STATUS_IGNORE);

    /* the kernel call is done inside */
    run_gpu(t1, b1, l1, r1, h, t, dt);

    if (t + dt > end_time) break;

    t = t + dt;
  }

#ifdef DEBUG
  printf("Rank: %d, before deinit_cuda\n", rank);
#endif
  /* final deinitialisation, freeing, copying back data */
  deinit_cuda(u);
#ifdef DEBUG
  printf("Rank: %d, after deinit_cuda\n", rank);
#endif

  snprintf(file, 20, "lake_f%d.dat", rank);
  print_heatmap(file, u, n, h);

  MPI_Finalize();
#ifdef DEBUG
  printf("Rank: %d, after finalize\n", rank);
#endif

  free(u_i0);
  free(u_i1);
  free(u);
  free(pebs);
  free(t1);
  free(b1);
  free(l1);
  free(r1);
  free(pebspos);

  return 0;
}

/* init routine for u0 and u1 variables */
void init(double *u, double *pebbles, int n)
{
  int i, j, idx;

  for(i = 0; i < n ; i++)
  {
    for(j = 0; j < n ; j++)
    {
      idx = j + i * n;
      u[idx] = f(pebbles[idx], 0.0);
    }
  }
}

/* broadcasting the initial pebbles positions by rank 0 */
void init_pebbles_bcast(int rank, int *pebspos, int pn, int n)
{
  int i, j, k, idx;

  if (rank == 0) {
    for (k = 0; k < pn; k++ ) {
      i = rand() % (n - 4) + 2;
      j = rand() % (n - 4) + 2;
      idx = j + i * n;
      pebspos[k] = idx;
    }
  }

  MPI_Bcast(pebspos, pn, MPI_INT, 0, MPI_COMM_WORLD);
}

/* function used to initialise */
double f(double p, double t)
{
  return -expf(-TSCALE * t) * p;
}

/* printing the values to a file */
void print_heatmap(char *filename, double *u, int n, double h)
{
  int i, j, idx;

  FILE *fp = fopen(filename, "w");  

  for( i = 0; i < n; i++ )
  {
    for( j = 0; j < n; j++ )
    {
      idx = j + i * n;
      fprintf(fp, "%f %f %f\n", i*h, j*h, u[idx]);
    }
  }
  
  fclose(fp);
} 
