/* vim: set syntax=c: */
/* vim: set smartindent: */
/* vim: set autoindent: */

/* Group info:
 *
 * sananth5 Srinath Krishna Ananthakrishnan
 * sramach6 Subramanian Ramachandran
 * vchandr3 Vinoth Kumar Chandra Mohan
 */
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <time.h>

#define __DEBUG

#define CUDA_CALL( err )     __cudaSafeCall( err, __FILE__, __LINE__ )
#define CUDA_CHK_ERR() __cudaCheckError(__FILE__,__LINE__)

/**************************************
* void __cudaSafeCall(cudaError err, const char *file, const int line)
* void __cudaCheckError(const char *file, const int line)
*
* These routines were taken from the GPU Computing SDK
* (http://developer.nvidia.com/gpu-computing-sdk) include file "cutil.h"
**************************************/
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef __DEBUG

#pragma warning( push )
#pragma warning( disable: 4127 ) // Prevent warning on do-while(0);
  do
  {
    if ( cudaSuccess != err )
    {
      fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
              file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }
  } while ( 0 );
#pragma warning( pop )
#endif  // __DEBUG
  return;
}

inline void __cudaCheckError( const char *file, const int line )
{
#ifdef __DEBUG
#pragma warning( push )
#pragma warning( disable: 4127 ) // Prevent warning on do-while(0);
  do
  {
    cudaError_t err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
      fprintf( stderr, "cudaCheckError() failed at %s:%i : %s.\n",
               file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }
    // More careful checking. However, this will affect performance.
    // Comment if not needed.
    /*err = cudaThreadSynchronize();
    if( cudaSuccess != err )
    {
      fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s.\n",
               file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }*/
  } while ( 0 );
#pragma warning( pop )
#endif // __DEBUG
  return;
}

#define TSCALE 1.0
#define VSQR 0.1

static double *dev_u;   // device u_n
static double *dev_u0;  // device u_0
static double *dev_u1;  // device u_1
static double *dev_peb; // devicep pebbles

/* these are the corner datas */
static double *dev_ti;
static double *dev_bi;
static double *dev_li;
static double *dev_ri;

static int n; /*n points*/
static int narea; /*grid area*/
static int nthreads; /*number of threads*/

/* initialisation routines to allocate and copy initial data into device memory */
extern "C" void init_cuda(int anarea, int an, double *u0, double *u1,
                          double *pebs, int anthreads, int pt, int pb,
                          int pl, int pr)
{
  n = an;
  narea = anarea;
  nthreads = anthreads;

  CUDA_CALL(cudaMalloc(&dev_u, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_u0, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_u1, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_peb, sizeof(double) * narea));

  if (pt != -1)
    CUDA_CALL(cudaMalloc(&dev_ti, sizeof(double) * n));
  if (pb != -1)
    CUDA_CALL(cudaMalloc(&dev_bi, sizeof(double) * n));
  if (pl != -1)
    CUDA_CALL(cudaMalloc(&dev_li, sizeof(double) * n));
  if (pr != -1)
    CUDA_CALL(cudaMalloc(&dev_ri, sizeof(double) * n));

  CUDA_CALL(cudaMemcpy(dev_u0, u0, sizeof(double) * narea, cudaMemcpyHostToDevice));
  CUDA_CALL(cudaMemcpy(dev_u1, u1, sizeof(double) * narea, cudaMemcpyHostToDevice));
  CUDA_CALL(cudaMemcpy(dev_peb, pebs, sizeof(double) * narea, cudaMemcpyHostToDevice));
}

/* the kernel ! */
__global__ void kernel(double *u, double *u0, double *u1, double *pebbles, double h,
                       double dt, double t, double *ti, double *bi, double *li,
                       double *ri)
{
  int cudaNumThreads = gridDim.x * gridDim.y * blockDim.x * blockDim.y;
	/*Calculate the index in the global memory*/
  int tid = threadIdx.x + blockIdx.x*blockDim.x +
            threadIdx.y*blockDim.x*gridDim.x +
            blockIdx.y*blockDim.x*blockDim.y*gridDim.x;

  double left, right, top, bottom;

  if (tid < cudaNumThreads) {
    u[tid] = 0;

    /* if i'm the edge threads in the global map, i don't have to compute anything! */
    if ((!threadIdx.x && !blockIdx.x && !li) ||
        (!threadIdx.y && !blockIdx.y && !ti) ||
        (threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1 && !ri) ||
        (threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1 && !bi)) {
      return;
    }

    /* determine my peer values, could be in the next block or could be
       across task boundaries */
    if (!threadIdx.x && !blockIdx.x) {
      left = li[blockIdx.y * blockDim.y + threadIdx.y];
    } else {
      left = u1[tid - 1];
    }

    if (!threadIdx.y && !blockIdx.y) {
      top = ti[blockIdx.x * blockDim.x + threadIdx.x];
    } else {
      top = u1[tid - blockDim.x*gridDim.x];
    }

    if (threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) {
      right = ri[blockIdx.y * blockDim.y + threadIdx.y];
    } else {
      right = u1[tid + 1];
    }

    if (threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) {
      bottom = bi[blockIdx.x * blockDim.x + threadIdx.x];
    } else {
      bottom = u1[tid + blockDim.x*gridDim.x];
    }

    u[tid] = 2*u1[tid] - u0[tid] + VSQR *(dt * dt) *((left + right + bottom + top -
               4 * u1[tid])/(h * h) + (-expf(-TSCALE*t)*pebbles[tid]));
  }

  return;
}

/* the entry from mpi to cuda, just copy the current boundary data and
   call the kernel, after it's done, prepare for the next step */
extern "C" void run_gpu(double *ti, double *bi, double *li, double *ri,
                        double h, double t, double dt)
{
  dim3 gridDim(n/nthreads, n/nthreads);
  dim3 blockDim(nthreads, nthreads);

  if (ti)
    CUDA_CALL(cudaMemcpy(dev_ti, ti, sizeof(double) * n, cudaMemcpyHostToDevice));
  if (bi)
    CUDA_CALL(cudaMemcpy(dev_bi, bi, sizeof(double) * n, cudaMemcpyHostToDevice));
  if (li)
    CUDA_CALL(cudaMemcpy(dev_li, li, sizeof(double) * n, cudaMemcpyHostToDevice));
  if (ri)
    CUDA_CALL(cudaMemcpy(dev_ri, ri, sizeof(double) * n, cudaMemcpyHostToDevice));

  kernel<<<gridDim, blockDim>>>(dev_u, dev_u0, dev_u1, dev_peb, h, dt, t, dev_ti, dev_bi, dev_li, dev_ri);

  CUDA_CALL(cudaMemcpy(dev_u0, dev_u1, sizeof(double) * narea, cudaMemcpyDeviceToDevice));
  CUDA_CALL(cudaMemcpy(dev_u1, dev_u,  sizeof(double) * narea, cudaMemcpyDeviceToDevice));
}

/* freeing memory and getting back the computed data from cuda */
extern "C" void deinit_cuda(double *u)
{
  CUDA_CALL(cudaMemcpy(u, dev_u, sizeof(double) * narea, cudaMemcpyDeviceToHost));

  CUDA_CALL(cudaFree(dev_u));
  CUDA_CALL(cudaFree(dev_u0));
  CUDA_CALL(cudaFree(dev_u1));
  CUDA_CALL(cudaFree(dev_peb));

  if (dev_ti)
    CUDA_CALL(cudaFree(dev_ti));
  if (dev_bi)
    CUDA_CALL(cudaFree(dev_bi));
  if (dev_li)
    CUDA_CALL(cudaFree(dev_li));
  if (dev_ri)
    CUDA_CALL(cudaFree(dev_ri));
}
