/* vim: set syntax=c: */
/* vim: set smartindent: */
/* vim: set autoindent: */

/* Group info:
 *
 * sananth5 Srinath Krishna Ananthakrishnan
 * sramach6 Subramanian Ramachandran
 * vchandr3 Vinoth Kumar Chandra Mohan
 */
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <time.h>

#define __DEBUG

#define CUDA_CALL( err )     __cudaSafeCall( err, __FILE__, __LINE__ )
#define CUDA_CHK_ERR() __cudaCheckError(__FILE__,__LINE__)

/**************************************
* void __cudaSafeCall(cudaError err, const char *file, const int line)
* void __cudaCheckError(const char *file, const int line)
*
* These routines were taken from the GPU Computing SDK
* (http://developer.nvidia.com/gpu-computing-sdk) include file "cutil.h"
**************************************/
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef __DEBUG

#pragma warning( push )
#pragma warning( disable: 4127 ) // Prevent warning on do-while(0);
  do
  {
    if ( cudaSuccess != err )
    {
      fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
              file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }
  } while ( 0 );
#pragma warning( pop )
#endif  // __DEBUG
  return;
}

inline void __cudaCheckError( const char *file, const int line )
{
#ifdef __DEBUG
#pragma warning( push )
#pragma warning( disable: 4127 ) // Prevent warning on do-while(0);
  do
  {
    cudaError_t err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
      fprintf( stderr, "cudaCheckError() failed at %s:%i : %s.\n",
               file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }
    // More careful checking. However, this will affect performance.
    // Comment if not needed.
    /*err = cudaThreadSynchronize();
    if( cudaSuccess != err )
    {
      fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s.\n",
               file, line, cudaGetErrorString( err ) );
      exit( -1 );
    }*/
  } while ( 0 );
#pragma warning( pop )
#endif // __DEBUG
  return;
}

#define TSCALE 1.0
#define VSQR 0.1

/* the kernel call */
__global__ void kernel(double *u, double *u0, double *u1, double *pebbles, double h, double dt, double t)
{
  int nthreads = gridDim.x * gridDim.y * blockDim.x * blockDim.y;
  /*Calculate the array index to be used by this thread*/
  int tid = threadIdx.x + blockIdx.x*blockDim.x +
            threadIdx.y*blockDim.x*gridDim.x +
            blockIdx.y*blockDim.x*blockDim.y*gridDim.x;

  int li, ri, ti, bi;

  if (tid < nthreads) {
    u[tid] = 0;

    /* if i'm not the corner threads, i'll calculate my value */
    if ((threadIdx.x || blockIdx.x) &&
        (threadIdx.y || blockIdx.y) &&
        (threadIdx.x != blockDim.x-1 || blockIdx.x != gridDim.x-1) &&
        (threadIdx.y != blockDim.y-1 || blockIdx.y != gridDim.y-1)) {
      /*Boundary info*/
      li = tid - 1;
      ri = tid + 1;
      ti = tid - blockDim.x*gridDim.x;
      bi = tid + blockDim.x*gridDim.x;

      u[tid] = 2*u1[tid] - u0[tid] + VSQR *(dt * dt) *((u1[li] + u1[ri] +
            u1[bi] + u1[ti] - 4 * u1[tid])/(h * h) + (-expf(-TSCALE*t)*pebbles[tid]));
    }
  }

  return;
}

/* the entry from lake.cu */
void run_gpu(double *u, double *u0, double *u1, double *pebbles, int n, double h, double end_time, int nthreads)
{
	cudaEvent_t kstart, kstop;
	float ktime;

	/* HW2: Define your local variables here */
  int narea = n * n;
  double *dev_u;
  double *dev_u0;
  double *dev_u1;
  double *dev_peb;

  double t = 0.;
  double dt = h / 2.;

  dim3 gridDim(n/nthreads, n/nthreads);
  dim3 blockDim(nthreads, nthreads);

  /* Set up device timers */  
	CUDA_CALL(cudaSetDevice(0));
	CUDA_CALL(cudaEventCreate(&kstart));
	CUDA_CALL(cudaEventCreate(&kstop));

	/* HW2: Add CUDA kernel call preperation code here */
  /* preparing some memory for the cuda kernel */
  CUDA_CALL(cudaMalloc(&dev_u, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_u0, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_u1, sizeof(double) * narea));
  CUDA_CALL(cudaMalloc(&dev_peb, sizeof(double) * narea));

	/* Start GPU computation timer */
	CUDA_CALL(cudaEventRecord(kstart, 0));

  CUDA_CALL(cudaMemcpy(dev_u0, u0, sizeof(double) * narea, cudaMemcpyHostToDevice));
  CUDA_CALL(cudaMemcpy(dev_u1, u1, sizeof(double) * narea, cudaMemcpyHostToDevice));
  CUDA_CALL(cudaMemcpy(dev_peb, pebbles, sizeof(double) * narea, cudaMemcpyHostToDevice));


	/* HW2: Add main lake simulation loop here */
  /* the loop to call the kernel.. subsequent calls of the kernel will sync across blocks
     memcpy the data for the next kernel call */
  while(1) {
    kernel<<<gridDim, blockDim>>>(dev_u, dev_u0, dev_u1, dev_peb, h, dt, t);

    CUDA_CALL(cudaMemcpy(dev_u0, dev_u1, sizeof(double) * narea, cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(dev_u1, dev_u,  sizeof(double) * narea, cudaMemcpyDeviceToDevice));

    if (t + dt > end_time) break;

    t = t + dt;
  }
	
  /* Stop GPU computation timer */
	CUDA_CALL(cudaEventRecord(kstop, 0));
	CUDA_CALL(cudaEventSynchronize(kstop));

	/* HW2: Add post CUDA kernel call processing and cleanup here */
  /* get back the data from the kernel and free the memory */
  CUDA_CALL(cudaMemcpy(u, dev_u, sizeof(double) * narea, cudaMemcpyDeviceToHost));
	
	CUDA_CALL(cudaEventElapsedTime(&ktime, kstart, kstop));
	printf("GPU computation took %f msec\n", ktime);

  CUDA_CALL(cudaFree(dev_u));
  CUDA_CALL(cudaFree(dev_u0));
  CUDA_CALL(cudaFree(dev_u1));
  CUDA_CALL(cudaFree(dev_peb));

	/* timer cleanup */
	CUDA_CALL(cudaEventDestroy(kstart));
	CUDA_CALL(cudaEventDestroy(kstop));
}
