all: lake lakempi

lake: lake.cu lakegpu.o
	nvcc lakegpu.o lake.cu -o ./lake -O2 -lm

lakegpu.o: lakegpu.cu
	nvcc -c lakegpu.cu -o lakegpu.o -Xcompiler -O2 -arch=sm_20

lakempi: lakempi.o lakempigpu.o 
	mpicc lakempigpu.o lakempi.o -L/usr/local/cuda-5.0/lib64/ -lcudart -lm -o lakempi

lakempi.o: lakempi.c
	mpicc -c lakempi.c -o lakempi.o

lakempigpu.o: lakempigpu.cu
	nvcc -c lakempigpu.cu -o lakempigpu.o -Xcompiler -O2 -arch=sm_20

clean: 
	rm -f lake lakempi lakegpu.o lakempi.o lakempigpu.o lake_*.dat lakempi.e* lakempi.out
